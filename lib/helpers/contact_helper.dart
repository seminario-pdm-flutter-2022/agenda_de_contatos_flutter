import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

const String contactTable = "contactTable";
const String idColumn = "idColumn";
const String nameColumn = "nameColumn";
const String emailColumn = "emailColumn";
const String phoneColumn = "phoneColumn";
const String imgColumn = "imgColumn";

class ContactHelper{
  //Declaração como SINGLETON: Os objetos instanciados de
  //ContactHelper são sempre o MESMO.
  static final ContactHelper _instance = ContactHelper.internal();
  factory ContactHelper() => _instance;

  ContactHelper.internal();

  late Database _db;
  Future<Database> get db async{
    _db = await initializeDatabase();
    return _db;
  }

  Future<Database> initializeDatabase() async{
    final databasePath = await getDatabasesPath();
    final path = join(databasePath, "contacts.db");

    return await openDatabase(path, version: 1, onCreate: (Database db, int newerVersion) async{
      await db.execute(
        "CREATE TABLE $contactTable($idColumn INTEGER PRIMARY KEY, $nameColumn TEXT, $emailColumn TEXT, $phoneColumn TEXT, $imgColumn TEXT)"
      );
    });
  }

  Future<Contact> saveContact(Contact contact) async{
    Database dbContact = await db;
    contact.id = await dbContact.insert(
        contactTable,
        Map<String, Object>.from(contact.toMap()),
        conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return contact;
  }

  Future<Contact?> getContact(int id) async{
    Database dbContact = await db;
    List<Map> response = await dbContact.query(
        contactTable,
        columns: ['idColumn', 'nameColumn', 'emailColumn', 'phoneColumn', 'imgColumn'],
        where: "$idColumn = ?",
        whereArgs: [id]
    );
    if(response.isNotEmpty){
      return Contact.fromMap(response.first);
    }else{
      return null;
    }
  }

  Future<int> deleteContact(int id) async{
    Database dbContact = await db;
    return await dbContact.delete(
      contactTable,
      where: "$idColumn = ?",
      whereArgs: [id]
    );
  }

  Future<int> updateContact(Contact contact) async{
    Database dbContact = await db;
    return await dbContact.update(
        contactTable,
        Map<String, Object>.from(contact.toMap()),
        where:"$idColumn = ?",
        whereArgs: [contact.id]
    );
  }

  Future<List> getAllContacts() async{
    Database dbContact = await db;
    List listMap = await dbContact.rawQuery("SELECT * from $contactTable;");
    List<Contact> listContact = <Contact>[];
    for(Map m in listMap){
      listContact.add(Contact.fromMap(m));
    }
    return listContact;
  }

  Future<int?> getNumber() async{
    Database dbContact = await db;
    return Sqflite.firstIntValue(await dbContact.rawQuery("SELECT COUNT(*) FROM $contactTable;"));
  }

  Future close() async{
    Database dbContact = await db;
    dbContact.close();
  }
}

class Contact{
  int id = -1;
  String name = "";
  String email = "";
  String phone = "";
  String img = "";

  Contact();

  Contact.fromMap(Map map){
    id = map[idColumn];
    name = map[nameColumn];
    email = map[emailColumn];
    phone = map[phoneColumn];
    img = map[imgColumn];
  }

  Map toMap(){
    Map<String, dynamic> map = {
      nameColumn: name,
      emailColumn: email,
      phoneColumn: phone,
      imgColumn: img
    };
    if(id != -1){
      map[idColumn] = id;
    }
    return map;
  }

  @override
  String toString() {
    return "Contact(id: $id, name:  $name, email: $email, phone: $phone, img: $img)";
  }
}
